## TeaRound Challenge

A simple app designed to allow users to create a tea round and allocate a random tea maker from the list of orders. I decided to build a small React app with Firebase as a simple persistance layer so that the tea round can be shared to the allocated tea maker. 

The app is currently deployed to netlify and is available at: https://hardcore-curie-fb6206.netlify.com/

Feel free to use it there and cross reference with the code, or just clone the repo and run it locally using `yarn start`

Please note, the app isn't production ready in a lot of ways and you may find some bugs that I didn't have time to address. For example, Firebase is currently set up with public credentials in `config.js` but this would ideally be set up with env parameters on a production build.

## Project overview

I wanted to use this as an opportunity to try out a few new things - primarily the new [React Hooks API](https://reactjs.org/docs/hooks-reference.html) and [React Context](https://reactjs.org/docs/context.html) with Firebase as a simple persistence layer. The method I used to allocate a tea maker was very simple - just randomly selecting a name from the orders array -so I decided to focus on some other areas of the app.

### Hooks & Context

Hooks are a new React feature that lets you use lifecycle methods and state inside function components. You can see Hooks in action throughout the app and I really like that they completely eliminated the need for class components and provide a much terser way of dealing with component side effects and clean up via the use of `useEffect`. Hooks also make using React Context for global state management much more appealing via the `useContext` hook which lets you set and interact with context as a state variable rather than using the Higher Order Component/Render prop pattern.  I also liked using Context to manage state, I found the pattern to update and manage global state much more intuitive and concise than using Redux. However, it would require slighly more work in ensuring immutability in a less trivial app.

### Firebase

The idea behind using Firebase was to be able to hold links to the tea round so that you would be able to share the tea round with the person who is making it. I decided to use firebase as it was fairly simple to set up and I didn't need to define any schemas. I was able to use just a few functions to manage the database state that you can see inside `services/firebase`. I set up simple anonymous authentication and r/w rules on the database so that excessive reads and writes could be prevented but this is something that could definitely be improved. This was the first time I have used firebase and there were a few more things that I could have looked into and improved. 

### Domain objects

I set up some simple domain objects in the `domain` folder which allowed for better typing over the orders and activeround objects. This is probably something that didn't need to be implemented for such a small app but I felt it would serve me well if I decided to implement more features.

### Styling

I set up some simple styling using styled components to encourage reuseability throughout the app, these can be found inside `components/styled` and used throughout the app. I also created a theme using the `ThemeProvider` components which would allow me to easily update colours and breakpoints if needed.

## Improvements/To do

• Better authentication and database cleanup  
• Randomise tea maker function is very simple - I could have looked into using a true random api, or implementing an algorithm to determine who makes the tea  
• Typechecking with proptypes   
• Set up a lambda or server side integration to add additional functionality such as sending email/text notifications  
• There's very little consideration to UX, this is something that could be improved  





