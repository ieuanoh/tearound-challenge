// Define global styles and theme

import { createGlobalStyle } from "styled-components";

const colours = {
  primary: "#477890",
  secondary: "#E0777D",
  tertiary: "#d3f7f7",
  accent: "#4C86A8",
  darkest: "#04061D",
  dark: "#171A48",
  light: "#E4E4E4",
  lightest: "#EEEEEE",
  black: "#000000",
  "grey-darkest": "#3d4852",
  "grey-darker": "#606f7b",
  "grey-dark": "#8795a1",
  grey: "#b8c2cc"
};

export const screens = {
  sm: "576px",
  md: "768px",
  lg: "992px",
  xl: "1200px"
};

export const theme = {
  ...colours,
  ...screens
};
