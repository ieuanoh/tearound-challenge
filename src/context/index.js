import React, { useState, useEffect } from "react";
import createRound from "domain/Round";
import createOrder from "domain/Order";
import {
  initDb,
  checkDbandUpdate,
  updateDb,
  clearDbItem,
  signIn,
  signOut
} from "services/firebase";
import { randomGenerator, generateUid } from "helpers";
import history from "services/history";

// Export Context so it can be used throughout the app
export const Context = React.createContext();

// Using a higher order component here means we can contain all the context
// logic in one place and just call it when we need to hook it up
const ContextProvider = ({ ...props }) => {
  // Using state to manage the context's appState allows React to trigger a re-render
  const [activeRound, setActiveRound] = useState(
    createRound("", generateUid())
  );
  const [orders, updateOrders] = useState([]);
  const [teaMaker, allocateMaker] = useState("");
  const [initialiseFirebase, setFirebaseInitialise] = useState(false);
  const [dbActive, setDbActive] = useState(false);
  const [dataLoading, setDataLoading] = useState(false);

  // The firebase setter function
  const databaseUpdate = () => {
    updateDb(activeRound, orders, teaMaker);
  };

  // On first mount, check for an id in the url, then look for it in firebase
  useEffect(() => {
    const urlId = window.location.pathname.split(/\/(?=.)/)[2];
    signIn();

    if (urlId) {
      setDataLoading(true);
      checkDbandUpdate(
        urlId,
        allocateMaker,
        updateOrders,
        setActiveRound,
        setDbActive,
        setDataLoading
      );
    }

    // Log user out on unmount
    return function cleanup() {
      signOut();
    };
  }, []);

  // These useEffect listen for state changes and update the database

  // Listen on initialiseFirebase, then initialise firebase
  useEffect(() => {
    if (initialiseFirebase) {
      initDb(activeRound, orders, teaMaker);
      setDbActive(true);
    }
  }, [initialiseFirebase]);

  // Subscribe to state changes with useEffect
  useEffect(() => {
    if (dbActive) {
      databaseUpdate();
    }
  }, [orders]);

  useEffect(() => {
    if (dbActive) {
      databaseUpdate();
    }
  }, [teaMaker]);

  // Initiate all of our app state and global functions
  // using internal state allows react to respond to updates

  const appState = {
    activeRound,
    orders,
    teaMaker,
    dataLoading,
    // Initialise a new tea round
    initialiseRound: function(name, initialOrder, newId) {
      // If specified, generate a new uid, otherwise use the one in state
      if (newId) {
        setActiveRound(activeRound => {
          return { ...activeRound, name, newId };
        });
        updateOrders([createOrder(name, initialOrder)]);
      } else {
        setActiveRound(activeRound => {
          return { ...activeRound, name };
        });
        updateOrders([...orders, createOrder(name, initialOrder)]);
      }
      // Triggers useEffect on initialiseFirebase to set the order to firebase
      setFirebaseInitialise(true);
    },
    // Add tea order
    addOrder: function(name, details) {
      updateOrders([...orders, createOrder(name, details)]);
    },
    // Remove order by index value
    removeOrder: index => {
      const ordersDupe = [...orders];
      ordersDupe.splice(index, 1);
      updateOrders(ordersDupe);
    },
    // Simple random generator to allocate a tea maker from the array of orders that exist
    allocateMaker: function() {
      const maker = randomGenerator(orders).name;
      allocateMaker(maker);
    },
    refreshState: function() {
      updateOrders([]);
      setActiveRound(createRound("", generateUid()));
      allocateMaker("");
      setFirebaseInitialise(false);
    },
    // Clear item from database and restore state
    newRound: function(withRedirect) {
      clearDbItem(activeRound.id);
      this.refreshState();
      withRedirect && history.push("/new");
    }
  };

  // Return a react provider component which we feed the state into and just render out any children
  return <Context.Provider value={appState}>{props.children}</Context.Provider>;
};

export default ContextProvider;
