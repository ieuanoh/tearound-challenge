import React from "react";
import { Router } from "react-router-dom";
import Provider from "components/App/Provider";
import Routes from "components/App/Routes";
import history from "services/history";
import "index.css";

const App = () => (
  <Router history={history}>
    <Provider>
      <Routes />
    </Provider>
  </Router>
);

export default App;
