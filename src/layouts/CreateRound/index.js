import React, { useState, useContext } from "react";
import { Context } from "context";
import history from "services/history";
import {
  ContentWrapper,
  InnerContainer
} from "components/Styled/ContentWrappers";
import { StyledHeader, StyledSub } from "components/Styled/StyledText";
import StyledForm from "components/Styled/StyledForm";
import StyledInput from "components/Styled/Input";
import Button from "components/Styled/Button";
import { generateUid } from "helpers";

const CreateRound = () => {
  const [name, setName] = useState("");
  const [order, setOrder] = useState("");
  const AppState = useContext(Context);

  // Create a new active round and initial order in the state,
  // Push new route with generated id
  const createRound = e => {
    e.preventDefault();
    // If a round is not active, initialise with the id on the store
    if (!AppState.activeRound.name) {
      AppState.initialiseRound(name, order);
      const roundId = AppState.activeRound.id;
      history.push(`/round/${roundId}`);
    } else {
      // If it's already been initiatied, generate a new uid and start a new round
      // This is for the case when users go back and initialise another round without clicking new round
      const newId = generateUid();
      AppState.initialiseRound(name, order, newId);
      const roundId = newId;
      history.push(`/round/${roundId}`);
    }
  };

  return (
    <ContentWrapper>
      <InnerContainer alignLeft>
        <StyledHeader>Start a TeaRound</StyledHeader>
        <StyledSub>
          Create a tea round and find out who gets to make it
        </StyledSub>
        <StyledForm onSubmit={e => createRound(e)}>
          <StyledInput
            onChange={e => setName(e.target.value)}
            type="text"
            placeholder="Jeff"
            value={name}
            name="Your name"
            required
          />
          <StyledInput
            onChange={e => setOrder(e.target.value)}
            type="textarea"
            placeholder="Your Tea Order"
            value={order}
            name="Your Order"
            required
          />
          <Button type="submit" label="Create Round" />
        </StyledForm>
      </InnerContainer>
    </ContentWrapper>
  );
};

export default CreateRound;
