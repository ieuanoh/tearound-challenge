import styled from "styled-components";

export const Container = styled.div`
  max-width: 35rem;
  margin: auto;
  text-align: center;
`;

export const StyledIcon = styled.img`
  width: 15rem;
`;
