import React from "react";
import {
  FixedWrapper,
  InnerContainer
} from "components/Styled/ContentWrappers";
import { StyledHeader, StyledSub } from "components/Styled/StyledText";
import { StyledIcon } from "./styled";
import Logo from "assets/tea.svg";

const OrderComplete = () => (
  <FixedWrapper>
    <InnerContainer>
      <StyledIcon src={Logo} />
      <StyledHeader>Congratulations!</StyledHeader>
      <StyledSub>You have completed your tea round!</StyledSub>
    </InnerContainer>
  </FixedWrapper>
);

export default OrderComplete;
