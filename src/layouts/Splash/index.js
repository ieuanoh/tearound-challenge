import React from "react";
import { Link } from "react-router-dom";
import {
  FixedWrapper,
  InnerContainer
} from "components/Styled/ContentWrappers";
import { StyledHeader, StyledSub } from "components/Styled/StyledText";
import Button from "components/Styled/Button";
import { StyledLogo } from "./styled";
import Logo from "assets/tea.svg";

const Splash = () => (
  <FixedWrapper>
    <InnerContainer>
      <StyledLogo src={Logo} />
      <StyledHeader>TeaRound</StyledHeader>
      <StyledSub>
        Create a new Tea Round, place your orders and find out who's making it.
      </StyledSub>
      <Link to="/new">
        <Button label="Start your tea round" />
      </Link>
    </InnerContainer>
  </FixedWrapper>
);

export default Splash;
