import React from "react";
import { Link } from "react-router-dom";
import {
  FixedWrapper,
  InnerContainer
} from "components/Styled/ContentWrappers";
import { StyledHeader, StyledSub } from "components/Styled/StyledText";
import Button from "components/Styled/Button";

const NotFound = () => (
  <FixedWrapper>
    <InnerContainer>
      <StyledHeader>Oops! We couldn't find that one</StyledHeader>
      <StyledSub>
        It's possible that your tea round ID is incorrect, or your round has
        already been completed!
      </StyledSub>
      <Link to="/new">
        <Button label="Start a new round" />
      </Link>
    </InnerContainer>
  </FixedWrapper>
);

export default NotFound;
