import React, { useContext } from "react";
import { Context } from "context";
import {
  ContentWrapper,
  ContentInner
} from "components/Styled/ContentWrappers";
import { StyledHeader, StyledSub } from "components/Styled/StyledText";
import Button from "components/Styled/Button";
import Flex from "components/Styled/Flex";
import OrderCard from "components/Content/OrderCard";
import AddOrder from "components/Content/AddOrder";
import history from "services/history";
import LoadingScreen from "components/App/LoadingScreen";

const ActiveRound = () => {
  const AppState = useContext(Context);
  const name = AppState.activeRound.name;
  const orders = AppState.orders;

  const allocateMaker = () => {
    AppState.allocateMaker();
    history.push(`/order/${AppState.activeRound.id}`);
  };

  if (!AppState.activeRound.name) {
    return <LoadingScreen timeOut={8000} />;
  }

  return (
    <ContentWrapper>
      <ContentInner>
        <StyledHeader>{`${name}`}'s Tea round</StyledHeader>
        <StyledSub>
          Input your orders and when you're done, click 'Assign a tea maker'. If
          you want to start again, click 'New round'
        </StyledSub>
        <Flex>
          {orders.map((order, index) => (
            <OrderCard key={Math.random()} index={index} order={order} />
          ))}
          <AddOrder />
        </Flex>
        {!AppState.teaMaker && (
          <Button
            marginRight
            onClick={() => allocateMaker()}
            label="Allocate tea maker"
          />
        )}
        <Button
          secondary
          onClick={() => AppState.newRound(true)}
          label="New Round"
        />
      </ContentInner>
    </ContentWrapper>
  );
};

export default ActiveRound;
