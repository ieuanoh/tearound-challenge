import React, { useContext } from "react";
import { Context } from "context";
import {
  ContentWrapper,
  ContentInner
} from "components/Styled/ContentWrappers";
import { StyledHeader, StyledSub } from "components/Styled/StyledText";
import Button from "components/Styled/Button";
import Flex from "components/Styled/Flex";
import OrderCard from "components/Content/OrderCard";
import LoadingScreen from "components/App/LoadingScreen";
import history from "services/history";

const Order = () => {
  const AppState = useContext(Context);
  const teaMaker = AppState.teaMaker;
  const orders = AppState.orders;

  const markComplete = () => {
    AppState.newRound(false);
    history.push("/ordercomplete");
  };

  if (!AppState.activeRound.name) {
    return <LoadingScreen timeOut={8000} />;
  }

  return (
    <ContentWrapper>
      <ContentInner>
        <StyledHeader>{`${teaMaker}`} is the allocated tea maker!</StyledHeader>
        <StyledSub>
          Here's the order! Make sure to share this link so they can see what
          they need to make
        </StyledSub>
        <Flex>
          {orders.map((order, index) => (
            <OrderCard
              key={Math.random()}
              noRemoveButton
              index={index}
              order={order}
            />
          ))}
        </Flex>
        <Button
          marginRight
          onClick={() => markComplete()}
          label="Mark as Complete"
        />
      </ContentInner>
    </ContentWrapper>
  );
};

export default Order;
