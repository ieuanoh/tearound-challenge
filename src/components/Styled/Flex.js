import styled from "styled-components";

export default styled.article`
  display: flex;
  flex-wrap: ${props => (props.nowrap ? "no-wrap" : "wrap")};
  align-items: start;
  justify-items: space-between;
  padding: 0.8rem 0;

  @media (max-width: ${props => props.theme.md}) {
    flex-direction: column;
  }
`;
