import React from "react";
import { Input, Container } from "./styled";

const StyledInput = ({ ...props }) => {
  return (
    <Container small={props.small}>
      {!props.noLabel && <label htmlFor={props.name}>{props.name}</label>}
      <Input
        small={props.small}
        type={props.type}
        onChange={props.onChange}
        placeholder={props.placeholder}
        value={props.value}
        name={props.name}
        ref={props.forwardedRef}
        error={props.error}
      />
    </Container>
  );
};

export default StyledInput;
