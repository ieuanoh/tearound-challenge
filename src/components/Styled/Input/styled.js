import styled from "styled-components";

export const Container = styled.div`
  margin: ${props => (props.small ? ".8rem 0" : "1rem 0")};
`;

export const Input = styled.input`
  outline: none;
  padding: ${props => (props.small ? ".8rem .5rem" : "1rem 0.5rem")};
  font-size: ${props => (props.small ? "1rem" : "1.6rem")};
  margin: ${props => (props.small ? "0 0 .5rem 0" : "0rem 0 1rem 0")};
  border: ${props => (props.error ? "2px solid red!important" : "none")};
  border-radius: ${props => (props.error ? "3px" : "1px")};
  background-image: none !important;
  color: black;
  letter-spacing: 0.05em;
  font-weight: 300;
  position: relative;
  background-color: transparent;
  width: 100%;
  border-bottom: 2px solid ${props => props.theme.primary};
  transition: 0.2s;
  box-sizing: border-box;

  :focus {
    border-bottom: 2px solid ${props => props.theme.secondary};
    transition: 0.2s;
    text-transform: none;
  }
`;
