import styled from "styled-components";

const StyledButton = styled.button`
  padding: ${props => (props.small ? ".5rem .8rem" : "1.2rem")};
  border: none;
  margin-right: ${props => (props.marginRight ? ".5rem!important" : "0")};
  margin: ${props => (props.small ? "0" : "1rem 0")};
  font-size: ${props => (props.small ? "1rem" : "1.2rem")};
  letter-spacing: 0.1rem;
  color: white;
  font-weight: bold;
  background-color: ${props =>
    props.secondary ? props.theme.secondary : props.theme.primary};
  transition: 0.4s all;
  border-radius: 5px;
  letter-spacing: 0.05em;
  font-weight: 300;
  font-family: "Lilita One", sans-serif;

  :hover {
    transform: translateY(-3px);
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.12), 0 2px 4px 0 rgba(0, 0, 0, 0.08);
    cursor: pointer;
  }
`;

export default StyledButton;
