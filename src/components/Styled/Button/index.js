import React from "react";
import PropTypes from "prop-types";
import StyledButton from "./styled";

const Button = ({ ...props }) => (
  <StyledButton {...props}>{props.label}</StyledButton>
);

Button.propTypes = {
  label: PropTypes.string.isRequired,
  secondary: PropTypes.bool,
  small: PropTypes.bool
};

Button.defaultProps = {
  secondary: false,
  small: false
};

export default Button;
