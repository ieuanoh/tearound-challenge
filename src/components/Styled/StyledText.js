import styled from "styled-components";

export const StyledHeader = styled.h1`
  font-size: 4rem;
  color: ${props => props.theme.primary};
  font-family: "Lilita One", sans-serif;
  font-weight: 600;
  letter-spacing: 0.5rem;
  margin: 1rem 0;

  @media (max-width: ${props => props.theme.lg}) {
    font-size: 2rem;
  }
`;

export const StyledSub = styled.p`
  margin: 0.8rem 0;
  font-size: 1.6rem;
  color: ${props => props.theme.greyDark};
  font-weight: 500;
  line-height: 1.5em;

  @media (max-width: ${props => props.theme.lg}) {
    font-size: 1rem;
  }
`;
