import styled from "styled-components";

export const FixedWrapper = styled.section`
  max-width: 50rem;
  margin: auto;
  padding: 0 2.5rem;
  display: flex;
  align-items: center;
  height: 100vh;
  position: fixed;
  left: 0;
  right: 0;
`;

export const ContentWrapper = styled.section`
  max-width: 50rem;
  margin: auto;
  padding: 5rem 2.5rem;
  display: flex;
  align-items: center;
  height: 100%;

  @media (max-width: ${props => props.theme.lg}) {
    padding: 3rem 1rem;
  }
`;

export const ContentInner = styled.article`
  min-width: 100%;
`;

export const InnerContainer = styled.div`
  max-width: 38rem;
  margin: auto;
  text-align: ${props => (props.alignLeft ? "left" : "center")};
`;
