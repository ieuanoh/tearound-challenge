import React, { useState, useEffect } from "react";
import { StyledLogo, Container } from "./styled";
import Logo from "assets/tea.svg";
import NotFound from "layouts/NotFound";

export default ({ ...props }) => {
  const [renderNotFound, updateNotFound] = useState(false);

  const loadTimeout = () => {
    updateNotFound(true);
  };

  useEffect(() => {
    const timeout = setTimeout(loadTimeout, props.timeOut);
    return function cleanup() {
      clearTimeout(timeout);
    };
  });

  if (renderNotFound) {
    return <NotFound />;
  }

  return (
    <Container>
      <StyledLogo src={Logo} alt="tea cup" />
    </Container>
  );
};
