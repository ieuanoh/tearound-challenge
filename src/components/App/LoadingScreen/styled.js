import styled, { keyframes } from "styled-components";

const pulse = keyframes`
    0% { opacity: 0.5; transform: scale(1); }
    45% { opacity: 1; transform: scale(1.1) }
    100% { opacity: 0.5; transform: scale(1); }
`;

export const Container = styled.div`
  position: fixed;
  z-index: 999;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: ${props => props.theme.secondary};
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const StyledLogo = styled.img`
  width: 8rem;
  animation: ${pulse} 1.5s linear infinite;
  transform-origin: center;
`;
