import React from "react";
import { Route, Switch } from "react-router-dom";

// Import layouts
import Splash from "layouts/Splash";
import NewRound from "layouts/CreateRound";
import ActiveRound from "layouts/ActiveRound";
import Order from "layouts/Order";
import OrderComplete from "layouts/OrderComplete";
import NotFound from "layouts/NotFound";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Splash} />
      <Route exact path="/new" component={NewRound} />
      <Route path="/round/:id" component={ActiveRound} />
      <Route path="/order/:id" component={Order} />
      <Route path="/ordercomplete" component={OrderComplete} />
      <Route render={NotFound} />
    </Switch>
  );
};

export default Routes;
