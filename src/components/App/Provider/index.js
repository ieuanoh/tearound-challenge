import React from "react";
import { ThemeProvider } from "styled-components";
import { theme } from "theme/global";
import ContextProvider from "context";

const Provider = ({ ...props }) => (
  <ContextProvider>
    <ThemeProvider theme={theme}>{props.children}</ThemeProvider>
  </ContextProvider>
);

export default Provider;
