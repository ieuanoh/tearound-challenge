import styled from "styled-components";

export const Container = styled.div`
  margin-bottom: 1rem;
  width: 33%;

  @media (max-width: ${props => props.theme.md}) {
    width: 100%;
  }
`;

export const Card = styled.div`
  background-color: white;
  border: 2px solid ${props => props.theme.light}
  border-radius: 5px;
  padding: 1rem;
  margin-right: 0.8rem;
`;

export const Name = styled.h3`
  font-size: 1.5rem;
  margin: 0.5rem 0;
  color: ${props => props.theme.primary};
  font-weight: bold;
`;

export const Order = styled.p`
  font-size: 1.2rem;
  color: ${props => props.theme.greyDark};
  font-weight: light;
`;
