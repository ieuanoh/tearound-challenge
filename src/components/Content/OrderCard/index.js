import React, { useContext } from "react";
import { Context } from "context";

import Button from "components/Styled/Button";
import { Container, Card, Name, Order } from "./styled";

const OrderCard = ({ ...props }) => {
  const { order, index } = props;
  const AppState = useContext(Context);

  const deleteOrder = () => {
    AppState.removeOrder(index);
  };

  return (
    <Container>
      <Card>
        <React.Fragment>
          <Name>{order.name}</Name>
          <Order>{order.orderDetails}</Order>
          {!props.noRemoveButton && (
            <Button
              secondary
              small
              label="Remove"
              onClick={() => deleteOrder()}
            >
              Delete
            </Button>
          )}
          {props.completedButton && (
            <Button
              small
              label="Mark as complete"
              onClick={() => deleteOrder()}
            >
              Delete
            </Button>
          )}
        </React.Fragment>
      </Card>
    </Container>
  );
};

export default OrderCard;
