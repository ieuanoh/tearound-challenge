import React, { useState, useContext } from "react";
import { Context } from "context";
import StyledForm from "components/Styled/StyledForm";
import StyledInput from "components/Styled/Input";
import Button from "components/Styled/Button";

import { Container, Card } from "./styled";

const AddOrder = () => {
  const AppState = useContext(Context);
  const [name, setName] = useState("");
  const [newOrder, setOrder] = useState("");
  const [error, setError] = useState(false);

  const addOrder = e => {
    e.preventDefault();
    if (name && newOrder) {
      AppState.addOrder(name, newOrder);
      setError(false);
      setName("");
      setOrder("");
    } else {
      setError(true);
    }
  };

  return (
    <Container>
      <Card>
        <React.Fragment>
          <StyledForm onSubmit={e => addOrder(e)}>
            <StyledInput
              small
              noLabel
              onChange={e => setName(e.target.value)}
              type="text"
              placeholder="Jeff"
              value={name}
              name="Your name"
              error={error}
              required
            />
            <StyledInput
              small
              noLabel
              onChange={e => setOrder(e.target.value)}
              type="textarea"
              placeholder="Your Tea Order"
              value={newOrder}
              name="Your Order"
              error={error}
              required
            />
            <Button small type="submit" label="Add Order" />
          </StyledForm>
        </React.Fragment>
      </Card>
    </Container>
  );
};

export default AddOrder;
