import styled from "styled-components";

export const Container = styled.div`
  margin-bottom: 1rem;
  width: 33%;

  @media (max-width: ${props => props.theme.md}) {
    width: 100%;
  }
`;

export const Card = styled.div`
  background-color: white;
  border: 2px solid ${props => props.theme.light}
  border-radius: 5px;
  padding: 1rem;
  margin-right: 0.8rem;
`;
