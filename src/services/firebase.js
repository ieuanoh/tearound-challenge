//  Boilerplate for initialising firebase realtime DB
import firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";
import config from "../config";

const firebaseConfig = firebase.initializeApp({
  apiKey: config.firebase.apiKey,
  authDomain: config.firebase.authDomain,
  databaseURL: config.firebase.databaseURL
});

// Initialise firebase, preventing it from doing it twice
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

// Sign in method using anonymous Auth in firebase
export const signIn = () => {
  firebase.auth().signInAnonymously();
};

export const signOut = () => {
  firebase.auth().signOut();
};

export const initDb = (activeRound, orders, teaMaker) => {
  const dbRef = firebase.database().ref(`/tearound/${activeRound.id}`);
  dbRef.set({ activeRound, orders, teaMaker });
};

export const updateDb = (activeRound, orders, teaMaker) => {
  const dbRef = firebase.database().ref(`/tearound/${activeRound.id}`);
  dbRef.set({ activeRound, orders, teaMaker });
};

export const clearDbItem = id => {
  const dbRef = firebase.database().ref(`/tearound/${id}`);
  dbRef.remove();
};

export const checkDbandUpdate = (
  id,
  allocateMaker,
  updateOrders,
  setActiveRound,
  setDbActive,
  setLoading
) => {
  const idRef = firebase.database().ref(`/tearound/${id}`);
  setLoading(true);
  idRef
    .once("value", snapshot => {
      if (snapshot.exists()) {
        const data = snapshot.val();
        setActiveRound(data.activeRound);
        updateOrders(data.orders);
        if (data.teaMaker) {
          allocateMaker(data.teaMaker);
        }
      } else {
        setActiveRound({});
        updateOrders([]);
      }
    })
    .then(() => {
      setDbActive(true);
      setLoading(false);
    })
    .catch(() => {
      setLoading(false);
    });
};

export default firebase;
