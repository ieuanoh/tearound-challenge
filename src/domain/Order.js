import Types from "./types";
import { generateUid } from "helpers";

const Order = {
  type: Types.ORDER,
  orderId: "",
  name: "",
  orderDetails: ""
};

const createOrder = (name, details) => {
  const order = Object.create(Order);
  order.orderId = generateUid();
  order.name = name;
  order.orderDetails = details;
  return order;
};

export default createOrder;
