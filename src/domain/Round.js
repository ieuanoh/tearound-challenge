import Types from "./types";

const Round = {
  type: Types.ROUND,
  id: "",
  name: ""
};

const createRound = (name, id) => {
  const round = Object.create(Round);
  round.id = id;
  round.name = name;
  return round;
};

export default createRound;
