export function randomGenerator(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

export function generateUid() {
  return (
    Math.random()
      .toString(36)
      // .replace(/[^a-z]+/g, "a")
      .substr(2, 10)
  );
}
